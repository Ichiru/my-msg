import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './chatApp.module.scss';

import DialogRoom from './DialogRoom';

const sn = classNames(styles);

export const ChatApp = () => {
  return (
    <div>
      <DialogRoom />
    </div>
  );
};
