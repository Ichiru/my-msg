import React from 'react';
import { useSelector } from 'react-redux';
import { classNames } from 'utilities/classNameUtility';
import styles from './infoBar.module.scss';

import { Link } from 'react-router-dom';
import ParticipantsDisplay from './ParticipantsDisplay';
// import onlineIcon from '../../icons/onlineIcon.png';
// import closeIcon from '../../icons/closeIcon.png';

const sn = classNames(styles);

export const InfoBar = ({ users }) => {
  const { room } = useSelector(state => state.setUserReducer);

  return (
    <div className={sn('info-bar')}>
      <div className={sn('info-bar__left-container')}>
        <Link to='/'>
          <button className={sn('left-container__button-to-back')}>
            leave
          </button>
        </Link>
        {/* <img className='online-icon' src={onlineIcon} alt='online icon' /> */}
        <h3 className={sn('left-container__room-name')}>{room}</h3>
      </div>
      <div className={sn('info-bar__right-container')}>
        <ParticipantsDisplay users={users} />
        {/* <a href='/'><img src={closeIcon} alt='close icon' /></a> */}
      </div>
    </div>
  );
};
