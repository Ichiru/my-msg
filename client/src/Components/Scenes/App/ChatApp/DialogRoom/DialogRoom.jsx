import React, { useState, useEffect } from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './dialogRoom.module.scss';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import Loader from 'Components/shared/Loader';
import InfoBar from './InfoBar';
import Messages from './Messages';
import MessageInput from './MessageInput';
//import MessageInput from './MessageInput';
import queryString from 'query-string';
//import { dispatchSocket } from 'redux/thunks/dispatchSocket';
import io from 'socket.io-client';

const sn = classNames(styles);

let socket;

export const DialogRoom = () => {
  const location = useLocation();
  const { userName, room } = queryString.parse(location.search);
  const [message, setMessage] = useState('');
  const [messages, addMessage] = useState([]);
  const [users, setUsers] = useState('');

  const ENDPOINT = 'localhost:5000';

  useEffect(() => {
    socket = io(ENDPOINT);

    socket.emit('joinRoom', { userName, room }, error => {
      console.log(`caught an error: ${error}`);
    });
  }, [ENDPOINT, location.search]);

  useEffect(() => {
    socket.on('userMessage', message => {
      addMessage([...messages, message]);
    });

    socket.on('roomData', ({ users }) => {
      setUsers(users);
    });

    return () => {
      socket.emit('disconnect');

      socket.off();
    };
  }, [messages]);

  const sendMessage = e => {
    e.preventDefault();

    if (message) {
      socket.emit('sendMessage', message, () => setMessage(''));
    }
  };

  return (
    <div className={sn('dialog-room')}>
      {true ? (
        <div className={sn('dialog-room__container container')}>
          <InfoBar users={users} />
          <Messages messages={messages} name={userName} />
          <MessageInput
            message={message}
            setMessage={setMessage}
            sendMessage={sendMessage}
          />
        </div>
      ) : (
        <Loader />
      )}
    </div>
  );
};
