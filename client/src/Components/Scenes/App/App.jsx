import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './app.module.scss';

import { HashRouter as Router, Route } from 'react-router-dom';
import ChatApp from './ChatApp';
import LoginPage from './LoginPage';

const sn = classNames(styles);

export const App = () => {
  return (
    <Router>
      <Route path='/' exact component={LoginPage} />
      <Route path='/chat-app' component={ChatApp} />
    </Router>
  );
};
