import { combineReducers } from 'redux';

import { loginReducer } from './loginReducer';
import { setUserReducer } from './setUserReducer';
import { socketReducer } from './socketReducer';

export default combineReducers({
  loginReducer,
  setUserReducer,
  socketReducer
});
