import { createSocket } from 'redux/actions/dialogRoomActions';

import io from 'socket.io-client';
const ENDPOINT = 'localhost:5000';

let socket;

export const dispatchSocket = () => dispatch => {
  socket = io(ENDPOINT);
  dispatch(createSocket(socket));
};
