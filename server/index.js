const express = require('express');
const socketio = require('socket.io');
const http = require('http');
const cors = require('cors');

const PORT = process.env.PORT || 5000;

const app = express();
const server = http.createServer(app);
const io = socketio(server);
const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');

const router = require('./router');

io.on('connection', socket => {
  socket.on('joinRoom', ({ userName, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, userName, room });

    if (error) {
      callback(error);
    }

    socket.join(user.room);

    socket.emit('userMessage', {
      user: '',
      text: `You've joined to ${user.room} room.`
    });

    socket.broadcast.to(user.room).emit('userMessage', {
      user: '',
      text: `${user.name}, has joined`
    });

    io.to(user.room).emit('roomData', {
      room: user.room,
      users: getUsersInRoom(user.room)
    });

    callback();
  });

  socket.on('sendMessage', (message, callback) => {
    const user = getUser(socket.id);

    io.to(user.room).emit('userMessage', {
      user: user.userName,
      text: message
    });

    callback();
  });

  socket.on('disconnect', () => {
    console.log('disconnecting');
    const user = removeUser(socket.id);

    if (user) {
      io.to(user.room).emit('userMessage', {
        user: '',
        text: `${user.userName} has left.`
      });
      io.to(user.room).emit('roomData', {
        room: user.room,
        users: getUsersInRoom(user.room)
      });
    }
  });
});

app.use(router);
app.use(cors());

server.listen(PORT, () => console.log('server is running on ' + PORT));
